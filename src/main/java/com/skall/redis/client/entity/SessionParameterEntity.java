
package com.skall.redis.client.entity;

import java.io.Serializable;

import org.springframework.data.annotation.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@RequiredArgsConstructor
@NoArgsConstructor
@ToString
public class SessionParameterEntity implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 7205211517204741450L;

    @Id
    @NonNull
    private String id;
}
