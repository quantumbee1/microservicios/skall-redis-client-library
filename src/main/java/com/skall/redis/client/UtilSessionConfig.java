
package com.skall.redis.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.skall.redis.client.util.RandomString;

@Configuration
@RefreshScope
public class UtilSessionConfig {
    @Value("${skall.session.id.length:20}")
    private Integer idLength;

    @Value("${skall.session.id.alphabet:abcdefghijklmnopqrstuvwxyz0123456789}")
    private String idAlphabet;

    @Bean("session-id")
    public RandomString configRandomStringSessionId() {
        return new RandomString(idLength, idAlphabet);
    }
}
