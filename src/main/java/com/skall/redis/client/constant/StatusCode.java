
package com.skall.redis.client.constant;

import lombok.Getter;

@Getter
public enum StatusCode {
    OK(0, "ok"),
    INTERNAL_SERVER_ERROR(-1, "internal server error"),
    BAD_REQUEST(-1, "bad request"),
    METHOD_NOT_ALLOWED(-1, "method not allowed"),
    UNSUPPORTED_MEDIA_TYPE(-1, "unsupported media type"),
    NOT_ACCEPTABLE(-1, "not acceptable"),
    NOT_FOUND(-1, "not found"),
    SERVICE_UNAVAILABLE(-1, "service unavailable"),
    UNAUTHORIZED(-1, "unauthorized"),
    ACCEPTED(-1, "accepted"),
    SESSION_INACTIVE(-2, "session inactive");

    private Integer code;
    private String description;

    private StatusCode(Integer code, String description) {
        this.code = code;
        this.description = description;
    }
}
