
package com.skall.redis.client.constant;

public class Constants {
    public static final String FILTER_NAME_VALIDATE_SESSION = "ValidateSessionFilter";

    public static final String HEADER_KEY_SESSION_ID = "sessionId";
}
