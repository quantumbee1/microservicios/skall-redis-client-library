
package com.skall.redis.client.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SessionStatus {
    INECTIVE("El usuario no tiene sesion activa"), LOGGED("El usuario tiene sesion activa");

    private String message;
}
