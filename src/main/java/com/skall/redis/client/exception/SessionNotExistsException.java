
package com.skall.redis.client.exception;

public class SessionNotExistsException extends RuntimeException {
    /**
     * 
     */
    private static final long serialVersionUID = -8624247535935523506L;

    public SessionNotExistsException(String message) {
        super(message);
    }
}
