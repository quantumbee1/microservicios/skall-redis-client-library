
package com.skall.redis.client.exception;

public class SessionIdWasNotCreatedException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = -7728934444968010317L;

    public SessionIdWasNotCreatedException(String message) {
        super(message);
    }
}
