
package com.skall.redis.client.repository;

import java.text.MessageFormat;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.skall.redis.client.entity.SessionParameterEntity;
import com.skall.redis.client.exception.SessionIdWasNotCreatedException;
import com.skall.redis.client.exception.SessionNotExistsException;
import com.skall.redis.client.util.RandomString;

@Component
public class SessionParameterRepository<T extends SessionParameterEntity> {
    private static final Logger logger = LoggerFactory.getLogger(SessionParameterRepository.class);

    private static final String ERROR_MESSAGE_SESSION_ID_WAS_NOT_CREATED =
            "El ID de session no se genero correctamente";
    private static final String PATTERN_SESSION_ID_AND_SERVICE_NAME =
            "session_id_{0}_service_name_{1}";

    @Autowired
    private RedisTemplate<String, T> template;

    @Qualifier("session-id")
    @Autowired
    private RandomString randomString;

    @Value("${spring.redis.session.application.timeout:40}")
    private Integer timeout;

    @Value("${spring.redis.session.application.renovate:true}")
    private Boolean renovate;

    @Value("${spring.application.name:skoll-app}")
    private String applicationName;

    public T create(final Class<T> sessionClazz) {
        try {
            final String id = randomString.nextString();
            T session = sessionClazz.newInstance();
            session.setId(id);

            this.save(session);

            return session;
        } catch (InstantiationException | IllegalAccessException e) {
            logger.error("SessionParameterRepositoryCreate",
                    ERROR_MESSAGE_SESSION_ID_WAS_NOT_CREATED, e);

            throw new SessionIdWasNotCreatedException(ERROR_MESSAGE_SESSION_ID_WAS_NOT_CREATED);
        }
    }

    public T create(final Class<T> sessionClazz, final String id) {
        try {
            final T session = sessionClazz.newInstance();
            session.setId(id);

            this.save(session);

            return session;
        } catch (InstantiationException | IllegalAccessException e) {
            logger.error("SessionParameterRepositoryCreate",
                    ERROR_MESSAGE_SESSION_ID_WAS_NOT_CREATED, e);

            throw new SessionIdWasNotCreatedException(ERROR_MESSAGE_SESSION_ID_WAS_NOT_CREATED);
        }
    }

    public T create(final T session) {
        try {
            session.setId(randomString.nextString());

            this.save(session);

            return session;
        } catch (Exception e) {
            logger.error("SessionParameterRepositoryCreate",
                    ERROR_MESSAGE_SESSION_ID_WAS_NOT_CREATED, e);

            throw new SessionIdWasNotCreatedException(ERROR_MESSAGE_SESSION_ID_WAS_NOT_CREATED);
        }
    }

    public T create(final T session, final String id) {
        try {
            session.setId(id);

            this.save(session);

            return session;
        } catch (Exception e) {
            logger.error("SessionParameterRepositoryCreate",
                    ERROR_MESSAGE_SESSION_ID_WAS_NOT_CREATED, e);

            throw new SessionIdWasNotCreatedException(ERROR_MESSAGE_SESSION_ID_WAS_NOT_CREATED);
        }
    }

    public void save(T session) {
        template.opsForValue().set(formatId(session.getId()), session, timeout, TimeUnit.MINUTES);
    }

    public T findById(String id) {
        T session = template.opsForValue().get(formatId(id));

        if (session != null) {
            if (renovate) {
                save(session);
            }
        } else {
            throw new SessionNotExistsException(
                    MessageFormat.format("La sesion para el ID [{0}] no fue encontrada", id));
        }

        return session;
    }

    public void delete(String id) {
        template.delete(formatId(id));

    }

    private String formatId(final String id) {
        return MessageFormat.format(PATTERN_SESSION_ID_AND_SERVICE_NAME, id, this.applicationName);
    }
}
